Task:

- create a self contained webservice that runs in a docker container:
  add a `docker-compose.yml` so i can start the service with 
  ```
  docker-compose up
  ``` 
  in the root of this project.
  
  for docs see https://docs.docker.com/compose/, https://docs.docker.com/glossary/?term=Dockerfile and https://docs.docker.com/ for docs on docker

- secure all BUT THE HEALTH endpoints with basic auth, the credentials should be able to be set via environment variable `BASIC_AUTH` in the docker-compose.yml

- implement health endpoint:
  it returns a static string like "ok"
```
GET /health
HTTP 200 OK
ok
```

- implement screenshot endpoint:
```
GET /screenshot?url={query escaped url}&width=1024
HTTP 200 OK + 
Raw bytes of screenshot of the webpage at {url} with displaywidth {width}
```

- implement meta endpoint:
```
GET /metadata?url={query escaped url}
HTTP 200 OK + 
```
json with url, response code, whois, Impressum if any (hint: look for any link in the page with content "impressum" or "imprint"), TLD, Domain, scheme. example:
```
{
  "url: "https://www.copytrack.com/de",
  "responseCode": 200,
  "whois":{
	  "Domain Name": "copytrack.com",
	  "Registry Domain ID": "77068625_DOMAIN_COM-VRSN",
	  "Registrant Name": "Marcus Schmitt"
	  ...
  },
  "imprint_url":"https://www.copytrack.com/de/impressum/",
  "tld":"com",
  "domain":"www.copytrack.com",
  "scheme":"https"
}
```
- sign up for gitlab, fork and create a pull reqest here

- try to write nice descriptive git commits

+ bonus add swagger 
